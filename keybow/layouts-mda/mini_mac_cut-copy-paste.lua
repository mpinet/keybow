require "keybow"

-- Keybow MINI volume controls --

function setup()
    keybow.use_mini()
    keybow.auto_lights(false)
    keybow.clear_lights()

    keybow.set_pixel(0, 0, 63, 0);    
    keybow.set_pixel(1, 0, 0, 63);    
    keybow.set_pixel(2, 63, 0, 0);

end

-- Key mappings --


-- 02 is left key --
function handle_minikey_02(pressed)
    if pressed then
        keybow.set_pixel(2, 255, 0, 0);
        keybow.set_modifier(keybow.LEFT_META, keybow.KEY_DOWN)
	keybow.tap_key("x", pressed)
        keybow.set_modifier(keybow.LEFT_META, keybow.KEY_UP)
    end
    if not pressed then
        keybow.set_pixel(2, 63, 0, 0);
    end
end



-- 01 is center key -- 
function handle_minikey_01(pressed)
    if pressed then
        keybow.set_pixel(1, 0, 0, 255);    
        keybow.set_modifier(keybow.LEFT_META, keybow.KEY_DOWN)
	keybow.tap_key("c", pressed)
        keybow.set_modifier(keybow.LEFT_META, keybow.KEY_UP)	
    end
    if not pressed then
        keybow.set_pixel(1, 0, 0, 63);    
    end
end



-- 00 is right key --
function handle_minikey_00(pressed)
    if pressed then
        keybow.set_pixel(0, 0, 255, 0);    
        keybow.set_modifier(keybow.LEFT_META, keybow.KEY_DOWN)
	keybow.tap_key("v", pressed)
        keybow.set_modifier(keybow.LEFT_META, keybow.KEY_UP)	
    end
    if not pressed then
        keybow.set_pixel(0, 0, 63, 0);    
    end
end


