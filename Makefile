#BOF

make.targets :
	@echo "available Make targets:"
	@$(MAKE) -pRrq -f $(firstword $(MAKEFILE_LIST)) : 2>/dev/null \
	| awk -v RS= -F: '/^# Implicit Rules/,/^# Finished Make data base/ {if ($$1 !~ "^[#.]") {print $$1}}' \
	| egrep -v '^make.targets$$' \
	| sed 's/^/    make    /' \
	| env LC_COLLATE=C sort



keybow-sync :
	rsync --recursive --verbose --dry-run --omit-dir-times --checksum keybow/ /Volumes/KEYBOW/

keybow-sync! :
	rsync --recursive --verbose           --omit-dir-times --checksum keybow/ /Volumes/KEYBOW/



keybow-diff :
	(cd keybow ; find * -type f) | sort | while read f; do echo $$f ; diff keybow/$$f /Volumes/KEYBOW/$$f 2>&1 | sed 's/^/    /' ; done

keybow-eject :
	diskutil eject /Volumes/KEYBOW/


#EOF
